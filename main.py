import os
import csv

# address file input folder here
path = r"..."
# address file output folder here
dest_path = r"..."
files_in_dir = os.listdir(path)


for file in files_in_dir:
    new_file = []
    new_file.append(file.split('.')[0] + "_1.csv")
    new_file.append(file.split('.')[0] + "_2.csv")
    new_path1 = os.path.join(dest_path, new_file[0])
    # find line where trajectory data start and separate them in two *.csv
    c = 0
    change = False
    with open(os.path.join(path, file), "r") as f:
        org_file = csv.reader(f)

        with open(new_path1, 'w+', newline='') as csvfile1:
            writer1 = csv.writer(csvfile1, delimiter=',')

            with open(os.path.join(dest_path, new_file[1]), 'w+', newline='') as csvfile2:
                writer2 = csv.writer(csvfile2, delimiter=',')

                for line in org_file:
                    try:
                        # You might want to change the keyword ('Trajactories') were the
                        if line[0] == 'Trajectories':
                            change = True

                    except IndexError:
                        pass

                    if not change:
                        writer1.writerow(line)
                    else:
                        writer2.writerow(line)
                    c += 1

    os.remove(os.path.join(path, file))
