# Vicon_CSV_splitter (Python)

 Using Vicon Nexus Software, different outputs (Trajactories, ModelOutput, etc.) get saved in one CSV-file as a horizontal concatenation. 

 In my opinion this isn't very intuitive, so I scripted a basic program, which split the input-CSV into two CVS-files using a 'keyword' (e.g.: 'Trajectories','ModelOutput') indicating a new section within the input-CSV.
